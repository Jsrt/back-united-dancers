const mongoose = require('mongoose')

let Schema = mongoose.Schema

let shoppingCardSchema = new Schema({
  comprador: {
    type: Schema.Types.ObjectId, ref: 'Usuario'
  },
  curso: {
    type: Schema.Types.ObjectId, ref: 'Curso'
  },
  total: {
    type: Number,
    required: [true, 'El valor total es necesario']
  }
})

module.exports = mongoose.model('Card', shoppingCardSchema)