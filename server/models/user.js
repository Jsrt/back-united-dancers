const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')


let rolesValidos = {
  values: ['ADMIN_ROLE', 'USER_ROLE', 'TEACH_ROLE'],
  message: '{VALUE} no es un rol válido'
}

let rangeValidos = {
  values: ['NEW', 'AFFILIATE', 'PARTNER'],
  message: '{VALUE}, no es un rango válido'
}

let Schema = mongoose.Schema

let usuarioSchema = new Schema({
  nombre: {
    type: String,
    required: [true, 'Los nombre son necesarios']
  },
  username: {
    type: String,
    required: true,
  },
  ciudad: {
    type: String
  },
  pais: {
    type: String
  },
  biografia: {
    type: String
  },
  numIdentificacion: {
    type: Number
  },
  email: {
    type: String,
    unique: true,
    required: [true, 'El correo es necesario']
  },
  password: {
    type: String,
    required: [true, 'La contraseña es obligatoria']
  },
  img: {
    type: String,
    required: true,
    default: 'https://thumbs.dreamstime.com/b/default-avatar-profile-image-vector-social-media-user-icon-potrait-182347582.jpg'
  },
  role: {
    type: String,
    default: 'USER_ROLE',
    enum: rolesValidos
  },
  estado: {
    type: Boolean,
    default: true
  },
  range: {
    type: String,
    enum: rangeValidos,
    default: 'NEW'
  },
  google: {
    type: Boolean,
    default: false
  },
  facebook: {
    type: Boolean,
    default: false
  },
  redFacebook: {
    type: String
  },
  redInstagram: {
    type: String
  },
  redTwiter: {
    type: String
  },
  redWhatsapp: {
    type: String
  }
})

usuarioSchema.methods.toJSON = function() {
  let user = this
  let userObject = user.toObject()
  delete userObject.password

  return userObject
}


usuarioSchema.plugin( uniqueValidator, { message: '{PATH} debe de ser único'})

module.exports = mongoose.model('Usuario', usuarioSchema)