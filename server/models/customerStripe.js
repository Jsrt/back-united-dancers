const mongoose = require('mongoose')

let Schema = mongoose.Schema

let customerStripe = new Schema({
  id: {
    type: Number
  },
  name: {
    type: String
  },
  email: {
    type: String
  },
  descripcion: {
    type: String
  },
  defaultSource: {
    type: Number
  },
  currency: {
    type: String
  }
})

module.exports = mongoose.model('CustomerStripe', customerStripe)