const mongoose = require('mongoose')

let seccionesValidos = {
  values: ['ESTRENOS', 'CLASES', 'GRATIS', 'PREMIUM'],
  message: '{VALUE} no es una seccion válida'
}

let Schema = mongoose.Schema

let cursoSchema = new Schema({
  nombre: {
    type: String,
    required: [true, 'El nombre del curso es necesario']
  },
  precio: {
    type: Number,
    required: [true, 'El precio es necesario']
  },
  descripcion: {
    type: String,
    default: ''
  },
  preRequisitos: {
    type: String,
    default: ''
  },
  dificultad: {
    type: String,
    required: [true, 'La dificultad es necesaria']
  },
  calificacion: {
    type: Number,
    default: 0
  },
  numVentas:{
    type: Number,
    default: 0
  },
  seccion: {
    type: String,
    default: 'ESTRENOS',
    enum: seccionesValidos
  },
  img: {
    type: String
  },
  usuario: {
    type: Schema.Types.ObjectId, ref: 'Usuario'
  }
})

module.exports = mongoose.model('Curso', cursoSchema)