const mongoose = require('mongoose')

let Schema = mongoose.Schema

let mediaSchema = new Schema({
  archivo: {
    type: String,
    required: [true, 'El Archivo es necesario']  
  },
  curso: {
    type: Schema.Types.ObjectId, ref: 'Curso'
  }
})

module.exports = mongoose.model('Media', mediaSchema)