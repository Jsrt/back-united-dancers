require('./config/config')

const express = require('express')
const mongoose = require('mongoose');
const path = require('path')
const cors = require('cors')

const app = express()
const bodyParser = require('body-parser')
 
// habilitar cors
app.use(cors())

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// habilitar la carpeta public
app.use(express.static(path.resolve(__dirname, '../public')))
 
// parse application/json
app.use(bodyParser.json())

// Configuración global de rutas
app.use(require('./routes/index'))

mongoose.connect(process.env.URLDB, 
                { useNewUrlParser: true, useCreateIndex: true },
                (err, res) => {
  if(err) throw err

  console.log('Base de datos ONLINE!')
})
 
app.listen(process.env.PORT, () => console.log('Escuchando en el puerto: ', 3000))