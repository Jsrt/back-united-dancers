const express = require('express')

const app = express()


app.use(require('./user'))
app.use(require('./login'))
app.use(require('./course'))
app.use(require('./upload'))
app.use(require('./imagenes'))
app.use(require('./stripe'))

module.exports = app