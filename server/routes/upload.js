const express = require('express')
const fileUpload = require('express-fileupload')
const app = express()

const Usuario = require('../models/user')
const Curso = require('../models/course')
const Media = require('../models/media')

const fs = require('fs')
const path = require('path')

//default options
app.use(fileUpload())

app.put('/upload/:tipo/:id', (req, res) => {

    let tipo = req.params.tipo
    let id = req.params.id

    if (!req.files)
      return res.status(400)
        .json({
          ok: false,
          err: {
            message: 'No se ha seleccionado ningún archivo'
          }
        })

    // valida tipo 
    let tiposValidos = ['cursos', 'usuarios', 'cursos_videos']

    if (tiposValidos.indexOf(tipo) < 0) {
      return res.status(400)
        .json({
          ok: false,
          err: {
            message: 'Los tipos permitidos son ' + tiposValidos.join(', ')
          }
        })
    }
    let archivo = req.files.archivo
    let nombreCortado = archivo.name.split('.')
    console.log(nombreCortado)

    let extension = nombreCortado[nombreCortado.length - 1]
    
    // Extensiones permitidas
    let extensionesPermitidas = ['png', 'jpg', 'gif', 'jpeg', 'mp4', 'svg']

    if (extensionesPermitidas.indexOf(extension) < 0) {
      return res.status(400)
        .json({
          ok: false,
          err:  {
            message: 'Las extensiones permitidas son ' + extensionesPermitidas.join(', '),
            ext: extension
          }
        })
    }
    
    // Cambiar nombre al archivo
    let nombreArchivo = `${ id }-${ tipo }-${ new Date().getMilliseconds()}.${ extension }`
    console.log('res', tipo, nombreArchivo)
    archivo.mv(`uploads/${tipo}/${nombreArchivo}`, (err) => {

      if (err) {
        return res.status(500)
        .json({
          ok: false,
          err
        })
      }
      
      if (tipo === 'cursos') {
         imagenCurso(id, res, nombreArchivo)
      } 

      if (tipo === 'usuarios') {
        // Aqui, Imagen cargada
        imagenUsuario(id, res, nombreArchivo)
      }

      if (tipo === 'cursos_videos') {
        imagenCursoVideo(id,res, nombreArchivo)
      }
    })
})

function imagenCursoVideo(id,res, nombreArchivo) {

  let media = new Media({
    archivo: nombreArchivo,
    curso: id
  })

  media.save((err, mediaDB) => {

      if (err) {

        borrarArchivo(nombreArchivo, 'cursos_video')

        return res.status(500).json({
          ok: false,
          err
        })
      }

      if (!mediaDB) {
        borrarArchivo(nombreArchivo, 'cursos_video')

        return res.status(400).json({
          ok: false,
          err
        })
      }

      res.json({
        ok: true,
        video: mediaDB
      })
  })
}

function imagenUsuario(id, res, nombreArchivo) {

  Usuario.findById(id, (err, usuarioDB) => {

    if (err) {
      borrarArchivo(nombreArchivo, 'usuarios')
      return res.status(500).json({
        ok: false,
        err
      })
    }

    if (!usuarioDB) {
      
      borrarArchivo(nombreArchivo, 'usuarios')

      return res.status(400).json({
        ok: false,
        err: {
          message: 'Usuario no existe'
        }
      })
    }

    borrarArchivo(usuarioDB.img, 'usuarios')

    usuarioDB.img = nombreArchivo

    usuarioDB.save((err, usuarioGuardado) => {
      
      res.json({
        ok: true,
        usuario: usuarioGuardado,
        img: nombreArchivo
      })
    })
  })
}

function imagenCurso(id, res, nombreArchivo) {

  Curso.findById(id, (err, cursoDB) => {

    if (err) {

      borrarArchivo(nombreArchivo, 'cursos')

      return res.status(500).json({
        ok: false,
        err
      })
    }

    if (!cursoDB) {
      borrarArchivo(nombreArchivo, 'cursos')

      return res.status(400).json({
        ok: false,
        err: {
          message: 'Curso no existe'
        }
      })
    }

    borrarArchivo(cursoDB.img, 'cursos')

    cursoDB.img = nombreArchivo

    cursoDB.save((err, cursoGuardado) => {
      
      res.json({
        ok: true,
        curso: cursoGuardado,
        img: nombreArchivo
      })
    })
  })
}

function borrarArchivo(nombreImagen, tipo) {

  let pathImage = path.resolve(__dirname, `../../uploads/${tipo}/${ nombreImagen }`)

    if (fs.existsSync(pathImage)) {
      fs.unlinkSync(pathImage)
    }
}

module.exports = app