const express = require('express')

let { verificaToken, verificaAdminRole, verificaTeachRole } = require('../middlewares/authentication')

let Curso = require('../models/course')
const app = express()

// ============================
// Mostrar todos los cursos
// ============================

app.get('/curso', (req, res) => {

  Curso.find({})
    .populate('usuario', 'username')
    .exec((err, cursos) => {

      if (err) {
          return res.status(500).json({
              ok: false,
              err
          });
      }

      res.json({
          ok: true,
          cursos
      });

  })
})

// ============================
// Mostrar un curso por ID
// ============================

app.get('/curso/:id', (req, res) => {

  let id = req.params.id

    Curso.findById(id)
        .populate('usuario', 'nombres img')
        .exec((err, cursoDB) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                })
            }

            if (!cursoDB) {
                return res.status(500).json({
                    ok: false,
                    err: {
                        message: 'El ID no es correcto'
                    }
                })
            }

            res.json({
                ok: true,
                curso: cursoDB
            })
        })
})

// =================================
// Mostrar un curso por ID USUARIO
// =================================

app.get('/curso/usuario/:id', verificaToken, (req, res) => {
    let id = req.params.id

    Curso.find({ usuario: id })
          .populate('usuario', 'nombres')
          .exec((err, cursoDB) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                })
            }

            if (!cursoDB) {
                return res.status(500).json({
                    ok: false,
                    err: {
                        message: 'El ID no es correcto'
                    }
                })
            }

            res.json({
                ok: true,
                curso: cursoDB
            }) 
        })
})

// ============================
// Crear nuevo curso
// ============================

app.post('/curso', [verificaToken, verificaTeachRole], (req, res) => {

  let body = req.body

  let curso = new Curso({
    nombre: body.nombre,
    precio: body.precio,
    descripcion: body.descripcion,
    preRequisitos: body.preRequisitos,
    dificultad: body.dificultad,
    calificacion: body.calificacion,
    numVentas: body.numVentas,
    seccion: body.seccion,
    usuario: req.usuario._id
  })

  curso.save((err, cursoDB) => {

    if (err) {
      return res.status(500).json({
          ok: false,
          err
      });
    }

    if (!cursoDB) {
        return res.status(400).json({
            ok: false,
            err
        });
    }

    res.json({
        ok: true,
        curso: cursoDB
    });

    })
})

// ============================
// Editar nuevo curso
// ============================

app.put('/curso/:id', verificaToken, (req, res) => {

  let id = req.params.id
  let body = req.body


  let cursoBody = {
    nombre: body.nombre,
    precio: body.precio,
    descripcion: body.descripcion
  }

  Curso.findByIdAndUpdate(id, cursoBody, { new: true, runValidators: true }, (err, cursoDB) => {

    if (err) {
      return res.status(500).json({
          ok: false,
          err
      });
    }

    if (!cursoDB) {
        return res.status(400).json({
            ok: false,
            err
        });
    }

    res.json({
        ok: true,
        curso: cursoDB
    });
  })
})

// ============================
// Eliminar curso
// ============================

app.delete('/curso/:id', [verificaToken, verificaAdminRole], (req, res) => {
  // solo un administrador puede borrar cursos
    let id = req.params.id

    Curso.findByIdAndRemove(id, (err, cursoDB) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!cursoDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'El id no existe'
                }
            });
        }

        res.json({
            ok: true,
            message: 'Curso Borrado'
        });

    });

})

module.exports = app;