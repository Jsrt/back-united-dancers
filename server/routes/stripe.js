const stripe = require('stripe')(process.env.STRIPE_API_KEY)
const express = require('express')

const app = express()

let CustomerStripe = require('../models/customerStripe')

app.post('/checkout', async (req, res) => {
  
  let body = req.body

  const customer = await stripe.customers.create({
    email: body.email,
    name: body.username
  })
  
  const charge = await stripe.charges.create({
    amount: '10000',
    currency: 'usd',
    customer: customer.id,
    descripcion: 'curso'
  })
  console.log(charge.id)
  res.send('recibido')
})

module.exports = app;